/*                                                                                                                                                                                                             
 * Author: Daya S Khudia (dskhudia@umich.edu)
 */

#ifndef VP_HOOKS_H
#define VP_HOOKS_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <inttypes.h>
#include <time.h>

/***** function prototypes ******/

#ifdef __cplusplus
extern "C" {
#endif

//
void VP_init(uint32_t num_instrs);
//
//
void VP_finish(void);
//
void VP_int(const uint64_t instr, const int64_t value);
void VP_float(const uint64_t instr, const float value);
void VP_double(const uint64_t instr, const double value);


#ifdef __cplusplus
}
#endif

#endif /* VP_HOOKS_H */
