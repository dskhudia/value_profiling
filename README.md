Value Profiling
=====

This repository contains an [llvm pass][llvmPass] to instrument code to perform value profiling and reading back the generated profile data. This llvm pass instruments the code to profile
instructions at the intermediate llvm IR level.

[llvmPass]: http://llvm.org/docs/WritingAnLLVMPass.html


Quick Start
----------

The code is tested to work with [llvm-3.3][llvm3.3].

[llvm3.3]:http://llvm.org/releases/download.html

###Compiling the Code

The code for value profiling instrumentation is developed by modifying the sample project from the llvm source directory. To compile it, please follow the following directions:

  * Change *LLVM\_SRC\_ROOT* and *LLVM\_OBJ\_ROOT* variables in *autoconf/configure.ac* to point to your llvm *src* and *obj* directory.
  * Run ./AutoRegen.sh in autoconf directory
  * Configure and build the project in the same-as-source or different directory


###Obtaining Value Profile Data

Assuming kanpsack.c is the source file that you want to profile. The following set of commands can be executed to obtain value profile data. These commands also assume
that llvm tools (opt, llc etc.) are in your path.

  $ opt -load <path-to-value-profiling-lib/valueProfiling.so> -vp-insts -insert-vp-profiling -insert-vp-init knapsack.bc -o knapsack.vp.bc

  $ llc -filetype=obj knapsack.vp.bc -o knapsack.vp.o

  $ g++ -o knapsack.vp knapsack.vp.o <build-dir>/vp-profiler/vp\_hooks.o

  $ ./knapsack.vp

The above set of commands will dump a file named *result.vp.out* that contains profile data.

###Obtaining Edge Profile Data

Before this value profile data can be read by pass, edge profile data is also required by the pass to read the value profile data. Edge profile data is required to get the total
execution count of different llvm IR instructions in the current program.

Edge profile data can be obtained by using following set of commands:

  $ opt -insert-edge-profiling knapsack.bc -o knapsack.prof.bc

  $ llc -filetype=obj knapsack.prof.bc -o knapsack.prof.o

  $ g++ -o knapsack.prof knapsack.prof.o <llvm-install-path>/lib/libprofile\_rt.so

  $ ./knapsack.prof

###Using Profile Data

Finally, a sample pass to read the profile data is provided (*-vp-analysis*). It can be used as follows:

  $ opt -load <path-to-value-profiling-lib/valueProfiling.so> -profile-loader -profile-info-file=llvmprof.out -vp-load-profile -vp-analysis knapsack.bc -o knapsack.out.bc

Please have a look at the *lib/usingVPData.cpp* file to know more about using the value profile data.

What Else
--------
A part of the instrumentation code is modelled after Loop-Aware-Memory-Profiling (LAMP) tool developed by [Thomas Mason][lamp].

[lamp]:http://liberty.cs.princeton.edu/Publications/mastersthesis_tmason.pdf
