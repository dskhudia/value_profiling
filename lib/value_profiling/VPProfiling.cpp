/*                                                                                                                                                                                                             
 * Author: Daya S Khudia (dskhudia@umich.edu)
 */

#define DEBUG_TYPE "VP"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/Pass.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/IntrinsicInst.h"
#include "VP/VPProfiling.h"


#include "llvm/ADT/Statistic.h"
#include "llvm/Support/InstIterator.h"
#include "llvm/Analysis/Passes.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/IRBuilder.h"

#include <iostream>
#include <set>

using namespace llvm;
using namespace std;

//This class counts the number of value generating instuctions. No modifications
STATISTIC(fpResTypeCount, "Number of instructions instrumented with fp result type");
STATISTIC(ptrResTypeCount, "Number of instructions instrumented with ptr result type");
STATISTIC(truncResTypeCount, "Number of instructions instrumented with result type truncated");
STATISTIC(zextResTypeCount, "Number of instructions instrumented with result type zero extended");
STATISTIC(sextResTypeCount, "Number of instructions instrumented with result type sign extended");
namespace {

  class InstCounter : public ModulePass {
  public:
    static char ID;
    static bool flag;
    bool runOnModule(Module &M);
    static uint64_t insts;
    InstCounter() : ModulePass(ID){
    }
    uint64_t getTotalInsts(){
      return insts;
    }
  };
}
char InstCounter::ID = 0;
//flag to insure we only count once
bool InstCounter::flag = false;
uint64_t InstCounter::insts = 0;

static RegisterPass<InstCounter> Y("vp-insts", 
                    "Count the number of value generating instructions");
ModulePass *llvm::createInstCounter(){
  return new InstCounter();
}

bool InstCounter::runOnModule(Module &M){
  if(flag == true){
    return false;
  }
  for(Module::iterator I = M.begin(), E = M.end(); I != E; I++){
    if(!I->isDeclaration()){
      //iterate over all instructions in the function
      Function &F = *I;
      DEBUG(dbgs() << "Instructions in the function: " << F.getName() << " are\n");
      for(inst_iterator Iter = inst_begin(F), End = inst_end(F); Iter != End; ++Iter){
        Instruction *pInst = &*Iter;
        DEBUG(dbgs() << "\t" << *pInst << "\n");
        DEBUG(dbgs() << "\t\t" << " type is:" << *(pInst->getType()) << "\n");
        //We only consider instructions that generate integer or floating point values
        //for value profiling. These are the only interesting ones value profiling. 
        if((pInst->getType()->isIntegerTy() || pInst->getType()->isFloatingPointTy()) && 
            (!isa<PHINode>(pInst) && !pInst->isTerminator())){
          insts++;
          DEBUG(dbgs() << "\t\tThis is integer or floating point type: " << pInst->getName() << "\n");
        }
        DEBUG(dbgs() << "\n");
      }
    }
  }
  flag = true;
  return false;
}
#undef DEBUG_TYPE
#define DEBUG_TYPE "VPProfiler"
char VPProfiler::ID = 0;
uint64_t VPProfiler::instr_id = -1;
DataLayout* VPProfiler::TD = NULL;

static RegisterPass<VPProfiler>
X("insert-vp-profiling", "Insert instrumentation for VP profiling");

FunctionPass *llvm::createVPProfilerPass(){
  return new VPProfiler();
}
int VPProfiler::getSize(Type* ty){
  int i = TD->getTypeSizeInBits(ty);
  switch(i){
    case 1:
      return 1;
    case 8:
      return 8;
    case 16:
      return 16;
    case 32:
      return 32;
    case 64:
      return 64;
    default:
      errs() << "getSize:" << i << " this is not handled\n";
      return -1;
  }
}

void VPProfiler::createVPDeclarations(Module &M){
  string fNames[] = {"VP_int", "VP_float", "VP_double"};
  LLVMContext &ctxt = M.getContext(); 
  vpFuncs[0] = M.getOrInsertFunction(fNames[0], Type::getVoidTy(ctxt), Type::getInt64Ty(ctxt),
             Type::getInt64Ty(ctxt), (Type *)0);
  vpFuncs[1] = M.getOrInsertFunction(fNames[1], Type::getVoidTy(ctxt), Type::getInt64Ty(ctxt),
             Type::getFloatTy(ctxt), (Type *)0);
  vpFuncs[2] = M.getOrInsertFunction(fNames[2], Type::getVoidTy(ctxt), Type::getInt64Ty(ctxt),
             Type::getDoubleTy(ctxt), (Type *)0);
}
bool VPProfiler::runOnFunction(Function &F){
  if(TD == NULL)
    TD = &getAnalysis<DataLayout>();

  if(vpFuncs[0] == NULL){
    Module &M = *(F.getParent());
    createVPDeclarations(M);
  }
  //if(F.getName() == "DeblockMb"){
  //return true;
  //}
  LLVMContext &ctxt = F.getContext();
  for(Function::iterator IF = F.begin(), IE = F.end(); IF != IE; ++IF){
    BasicBlock &BB = *IF;
    IRBuilder<> builder(&BB);
    //DEBUG(dbgs() << "CurrBB: \n");
    //DEBUG(dbgs() << BB);
    //if(F.getName() == "_ZN10FileStreamC2EPc"){
        //for breakpoint
        //BB.dump();
        //}
    //Terminator instructions typically yield a void value except invoke instruction.
    //We can't profile a terminator instruction
    for(BasicBlock::iterator I = BB.begin(), E = BB.end(); I != E; ++I){
      Instruction &curInst = *I;
      //This condition must be same as the one in instruction counting.
      //if the current instruction generates integer/floating point values, instrument it.
      if(!isa<PHINode>(curInst) && !curInst.isTerminator()){
          if(curInst.getType()->isIntegerTy()){
            //get the generated value type and instrument for that type
            Type* resultType = curInst.getType();
            BasicBlock::iterator insrtPt = curInst;
            insrtPt++;
            builder.SetInsertPoint(insrtPt);
            vector<Value*> Args(2);
            Args[0] = ConstantInt::get(Type::getInt64Ty(ctxt), ++instr_id);
            //there is no sign information associated with types in llvm. Operation on the values 
            //decide whether to use them as sign or unsigned.
            //Treat i1 as unsigned and others as signed.
            //int1
            if(TD->getTypeSizeInBits(resultType) < 32){
              Args[1] = new ZExtInst(&curInst, Type::getInt64Ty(ctxt), "vpZExt", &(*insrtPt));
              zextResTypeCount++;
              I++;
            }
            else if(TD->getTypeSizeInBits(resultType) < 64){
              Args[1] = new SExtInst(&curInst, Type::getInt64Ty(ctxt), "vpSExt", &(*insrtPt));
              sextResTypeCount++;
              I++;
            }
            else if(TD->getTypeSizeInBits(resultType) == 64){
              Args[1] = &curInst;
            }
            //ignore others
            else{
              Args[1] = new SExtInst(&curInst, Type::getInt64Ty(ctxt), "vpSExt", &(*insrtPt));
              zextResTypeCount++;
              I++;
            }
            builder.CreateCall2(vpFuncs[0], Args[0], Args[1], "");
          }
          else if(curInst.getType()->isFloatingPointTy()){
            BasicBlock::iterator insrtPt = curInst;
            insrtPt++;
            builder.SetInsertPoint(insrtPt);
            vector<Value*> Args(2);
            Args[0] = ConstantInt::get(Type::getInt64Ty(ctxt), ++instr_id);
            switch (curInst.getType()->getTypeID()) {
              case Type::FloatTyID:{
                Args[1] = &curInst;
                builder.CreateCall2(vpFuncs[1], Args[0], Args[1], "");
                break;
              }
              case Type::DoubleTyID:{
                Args[1] = &curInst;
                builder.CreateCall2(vpFuncs[2], Args[0], Args[1], "");
                break;
              }
              default:{
                //nothing to be done
              }
            }
          }
          else{
            //do nothing
          }
      }
    }
    //if(F.getName() == "_ZN10FileStreamC2EPc"){
    //BB.dump();    
    //}

  }
  return true;
}
#undef DEBUG_TYPE
#define DEBUG_TYPE "VP_init"
//The data from the class InstCounter is used is this
namespace {
  class VPInit : public ModulePass{
    bool runOnModule(Module &M);

    public:
      virtual void getAnalysisUsage(AnalysisUsage &AU) const{
        AU.addRequired<InstCounter>();
      }
      static char ID;
      VPInit() : ModulePass(ID){
      }
  };
}
char VPInit::ID = 0;
static RegisterPass<VPInit>
V("insert-vp-init", "Insert initialization for VP profling");

ModulePass *createVPInitPass(){
  return new VPInit();
}
bool VPInit::runOnModule(Module &M){
  for(Module::iterator IF = M.begin(), E = M.end(); IF !=E; ++IF){
    Function& F = *IF;
    if(F.getName() == "main"){
      const char* FnName = "VP_init";
      LLVMContext &ctxt = M.getContext(); 
      InstCounter& instCnt = getAnalysis<InstCounter>();
      uint64_t cnt = instCnt.getTotalInsts();

      Constant *InitFn = M.getOrInsertFunction(FnName, Type::getVoidTy(ctxt), Type::getInt32Ty(ctxt), (Type *)0);
      BasicBlock& entry = F.getEntryBlock();
      BasicBlock::iterator InsertPos = entry.begin();
      while (isa<AllocaInst>(InsertPos)) ++InsertPos;

      std::vector<Value*> Args(1);
      Args[0] = ConstantInt::get(Type::getInt32Ty(ctxt), cnt, false);
      CallInst::Create(InitFn, Args, "", InsertPos);
      return true;

    }
  }
  return false;
}
#undef DEBUG_TYPE
#define DEBUG_TYPE "VP"
